package com.example.preexam

class ReciboNomina(
    val numeroRecibo: String,
    val nombre: String,
    val horasNormales: Double,
    val horasExtra: Double,
    val puesto: Int,
    val impuesto: Double = 0.16
) {
    companion object {
        const val PUESTO_AUXILIAR = 1
        const val PUESTO_ALBANIL = 2
        const val PUESTO_INGENIERO_OBRA = 3
        const val PAGO_BASE = 200.0
    }

    fun calcularSubtotal(): Double {
        val pagoBase = when (puesto) {
            PUESTO_AUXILIAR -> PAGO_BASE * 1.2
            PUESTO_ALBANIL -> PAGO_BASE * 1.5
            PUESTO_INGENIERO_OBRA -> PAGO_BASE * 2.0
            else -> PAGO_BASE
        }

        val pagoHorasNormales = pagoBase * horasNormales
        val pagoHorasExtra = pagoBase * horasExtra * 2

        return pagoHorasNormales + pagoHorasExtra
    }

    fun calcularImpuesto(): Double {
        return calcularSubtotal() * impuesto
    }
}
