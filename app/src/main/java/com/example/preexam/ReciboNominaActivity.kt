package com.example.preexam




import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.RadioButton
import android.widget.RadioGroup
import android.widget.TextView
import java.math.BigDecimal
import java.math.RoundingMode
import com.example.preexam.R
import android.app.AlertDialog
import android.widget.Toast
import com.example.preexam.ReciboNomina
class ReciboNominaActivity : AppCompatActivity() {

    private lateinit var editNumeroRecibo: EditText
    private lateinit var editNombre: EditText
    private lateinit var editHorasNormales: EditText
    private lateinit var editHorasExtra: EditText
    private lateinit var radioGroupPuesto: RadioGroup
    private lateinit var radioAuxiliar: RadioButton
    private lateinit var radioAlbanil: RadioButton
    private lateinit var radioIngObra: RadioButton
    private lateinit var btnCalcular: Button
    private lateinit var btnLimpiar: Button
    private lateinit var btnRegresar: Button
    private lateinit var textSubtotal: TextView
    private lateinit var textImpuesto: TextView
    private lateinit var textTotalPagar: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recibo_nomina)

        editNumeroRecibo = findViewById(R.id.editNumeroRecibo)
        editNombre = findViewById(R.id.editNombre)
        editHorasNormales = findViewById(R.id.editHorasNormales)
        editHorasExtra = findViewById(R.id.editHorasExtra)
        radioGroupPuesto = findViewById(R.id.radioGroupPuesto)
        radioAuxiliar = findViewById(R.id.radioAuxiliar)
        radioAlbanil = findViewById(R.id.radioAlbanil)
        radioIngObra = findViewById(R.id.radioIngObra)
        btnCalcular = findViewById(R.id.btnCalcular)
        btnLimpiar = findViewById(R.id.btnLimpiar)
        btnRegresar = findViewById(R.id.btnRegresar)
        textSubtotal = findViewById(R.id.textSubtotal)
        textImpuesto = findViewById(R.id.textImpuesto)
        textTotalPagar = findViewById(R.id.textTotalPagar)

        btnCalcular.setOnClickListener {
            calcularNomina()
        }

        btnLimpiar.setOnClickListener {
            limpiarCampos()
        }

        btnRegresar.setOnClickListener {
            regresar()
        }

    }

    private fun calcularNomina() {
        val numeroRecibo = editNumeroRecibo.text.toString()
        val nombre = editNombre.text.toString()
        val horasNormales = editHorasNormales.text.toString()
        val horasExtra = editHorasExtra.text.toString()

        if (numeroRecibo.isEmpty() || nombre.isEmpty() || horasNormales.isEmpty() || horasExtra.isEmpty()) {
            Toast.makeText(this, "Por favor, completa todos los campos", Toast.LENGTH_SHORT).show()
            return
        }

        val puesto = when (radioGroupPuesto.checkedRadioButtonId) {
            R.id.radioAuxiliar -> 1
            R.id.radioAlbanil -> 2
            R.id.radioIngObra -> 3
            else -> 0
        }

        val subtotal = calcularSubtotal(puesto, horasNormales.toInt(), horasExtra.toInt())
        val impuesto = calcularImpuesto(subtotal)
        val totalPagar = subtotal - impuesto

        textSubtotal.text = "Subtotal: ${subtotal.setScale(2, RoundingMode.HALF_UP)}"
        textImpuesto.text = "Impuesto: ${impuesto.setScale(2, RoundingMode.HALF_UP)}"
        textTotalPagar.text = "Total a Pagar: ${totalPagar.setScale(2, RoundingMode.HALF_UP)}"
    }


    private fun calcularSubtotal(puesto: Int, horasNormales: Int, horasExtra: Int): BigDecimal {
        val pagoBase = BigDecimal(200)
        val pagoPorHora = when (puesto) {
            1 -> pagoBase * BigDecimal(1.2)
            2 -> pagoBase * BigDecimal(1.5)
            3 -> pagoBase * BigDecimal(2)
            else -> pagoBase
        }

        val subtotalNormales = pagoPorHora * BigDecimal(horasNormales)
        val subtotalExtra = pagoPorHora * BigDecimal(2) * BigDecimal(horasExtra)

        return subtotalNormales + subtotalExtra
    }

    private fun calcularImpuesto(subtotal: BigDecimal): BigDecimal {
        val porcentajeImpuesto = BigDecimal(0.16)
        return subtotal * porcentajeImpuesto.setScale(2, RoundingMode.HALF_UP)
    }

    private fun limpiarCampos() {
        editNumeroRecibo.text.clear()
        editNombre.text.clear()
        editHorasNormales.text.clear()
        editHorasExtra.text.clear()
        radioGroupPuesto.clearCheck()
        textSubtotal.text = "Subtotal:"
        textImpuesto.text = "Impuesto:"
        textTotalPagar.text = "Total a Pagar:"
    }


    private fun regresar() {
        val confirmar = AlertDialog.Builder(this)
        confirmar.setTitle("Recibo Nomina")
        confirmar.setMessage("¿Deseas regresar?")
        confirmar.setPositiveButton("Confirmar") { _, _ -> finish() }
        confirmar.setNegativeButton("Cancelar", null)
        confirmar.show()
    }
}
