package com.example.preexam

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import android.content.Intent
import com.example.preexam.ReciboNominaActivity


class MainActivity : AppCompatActivity() {
    private lateinit var btnEnter: Button
    private lateinit var btnExit: Button
    private lateinit var txtUsuario: EditText


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        iniciarComponentes()
        btnEnter.setOnClickListener { login() }
        btnExit.setOnClickListener {
            salir()
        }
    }

    private fun iniciarComponentes() {
        btnEnter = findViewById(R.id.btnEnter)
        btnExit = findViewById(R.id.btnExit)

        txtUsuario = findViewById(R.id.txtUsuario)
    }

    private fun login() {
        val strUsuario: String = resources.getString(R.string.usuario)


        if (txtUsuario.text.toString() == strUsuario ) {
            val intent = Intent(this@MainActivity, ReciboNominaActivity::class.java)
            intent.putExtra("usuario", strUsuario)
            startActivity(intent)
        } else {
            Toast.makeText(this, "Usuario no valido", Toast.LENGTH_LONG).show()
        }
    }
    private fun salir() {
        finish()
    }
}